FROM php:8.0.9-fpm-alpine

RUN apk add --no-cache postgresql-dev \
  && docker-php-ext-install pdo_pgsql

RUN mkdir /app
COPY /src /app
